import './App.css';
import legislatives2022round2 from './data/data';

function App() {
console.log(legislatives2022round2);

  return (
    <div className="App">
      <h1>Circonscriptions</h1>
      <ul>
      {legislatives2022round2.map((item) => (
        <li>{item.candidat}</li>
      ))}
      </ul>
    </div>
  );
}

export default App;
